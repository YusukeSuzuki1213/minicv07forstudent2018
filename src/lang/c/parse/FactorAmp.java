package lang.c.parse;
import java.io.PrintStream;

import lang.*;
import lang.c.*;

public class FactorAmp extends CParseRule {
    private CToken amp,tk,ntk;
    private CParseRule factorAmp;
    public FactorAmp(CParseContext pcx) {
    }
    public static boolean isFirst(CToken tk) {
        return tk.getType() == CToken.TK_AMPERSAND;
    }
    public void parse(CParseContext pcx) throws FatalErrorException {
        // ここにやってくるときは、必ずisFirst()が満たされている
        CTokenizer ct = pcx.getTokenizer();
        amp = ct.getCurrentToken(pcx);
        tk = ct.getNextToken(pcx);
        if (Number.isFirst(tk)) {
            factorAmp = new Number(pcx);
            factorAmp.parse(pcx);
        } else if (Primary.isFirst(tk)) {
            factorAmp = new Primary(pcx);
            factorAmp.parse(pcx);
        } else {
            pcx.fatalError(tk.toExplainString() + "&の後ろは.....何がくるか考えろよ！");
        }

    }

    public void semanticCheck(CParseContext pcx) throws FatalErrorException {
        if (factorAmp instanceof Primary) {
            if (((Primary) factorAmp).getChild() instanceof PrimaryMult) {
                pcx.fatalError( tk.toExplainString()+"'&'の後に'*識別子'は許可されていません。");//GCCではコンパイル通った。
            }
        }
        if (factorAmp != null) {
            factorAmp.semanticCheck(pcx);
            setCType(CType.getCType(CType.T_pint));
            setConstant(true);
        }
    }

    public void codeGen(CParseContext pcx) throws FatalErrorException {
        PrintStream o = pcx.getIOContext().getOutStream();
        o.println(";;; factorAmp starts");
        if (factorAmp != null) factorAmp.codeGen(pcx);
        o.println(";;; factorAmp completes");
    }
}
