package lang.c.parse;

import java.io.PrintStream;
import java.util.ArrayList;

import lang.*;
import lang.c.*;

public class IntDecl extends CParseRule {
    private CToken int_,tk;
    private CParseRule declItem;
    private ArrayList<CParseRule> declItems = new ArrayList<CParseRule>();
    public IntDecl(CParseContext pcx) {
    }
    public static boolean isFirst(CToken tk) {
        return tk.getType() == CToken.TK_INT;
    }
    public void parse(CParseContext pcx) throws FatalErrorException {
        CTokenizer ct = pcx.getTokenizer();
        int_ = ct.getCurrentToken(pcx);// 'int'が取り出されるはず

        tk = ct.getNextToken(pcx);//declItemが取り出さる
        if (DeclItem.isFirst(tk)) {
            declItem = new DeclItem(pcx);
            declItem.parse(pcx);
            declItems.add(declItem);
        } else {
            pcx.fatalError(tk.toExplainString() + "'int'の後ろはdeclItemです");
        }

        tk = ct.getCurrentToken(pcx);//, or ;が取り出される
        while (tk.getType() == CToken.TK_COMMA){
            tk = ct.getNextToken(pcx);//declItemが取り出さる
            if (DeclItem.isFirst(tk)) {
                declItem = new DeclItem(pcx);
                declItem.parse(pcx);
                declItems.add(declItem);
            } else {
                pcx.fatalError(tk.toExplainString() + "','の後ろはdeclItemです");
            }
            tk = ct.getCurrentToken(pcx);
        }

        if (tk.getType() == CToken.TK_SEMI) {
            ct.getNextToken(pcx);
        } else {
            pcx.fatalError(tk.toExplainString() + "declItemの後ろは';'です");
        }
    }

    public void semanticCheck(CParseContext pcx) throws FatalErrorException {
        for(CParseRule d : declItems){
            if (d != null) { d.semanticCheck(pcx); }
        }
    }

    public void codeGen(CParseContext pcx) throws FatalErrorException {
        PrintStream o = pcx.getIOContext().getOutStream();
        o.println(";;; IntDecl starts");
        for(CParseRule d : declItems){
            if (d != null) { d.codeGen(pcx); }
        }
        o.println(";;; IntDecl completes");
    }
}
