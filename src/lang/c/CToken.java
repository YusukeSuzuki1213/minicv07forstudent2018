package lang.c;

import lang.SimpleToken;

public class CToken extends SimpleToken {
	public static final int TK_PLUS			= 2;	// +
	public static final int TK_MINUS		= 3;	// -
	public static final int TK_MULT		    = 4;	// *
	public static final int TK_DIV		    = 5;	// /
	public static final int TK_AMPERSAND	= 6;	// &(アドレス)
	public static final int TK_OCTAL		= 7;	// 8進数
	public static final int TK_DECIMAL		= 8;	// 10進数
	public static final int TK_HEXADECIMAL	= 9;	// 16進数
	public static final int TK_LPAR	        = 10;	// (
	public static final int TK_RPAR	        = 11;	// )
	public static final int TK_LBRA	        = 12;	// [
	public static final int TK_RBRA	        = 13;	// ]
	public static final int TK_ASSIGN	    = 14;	// =
	public static final int TK_SEMI	        = 15;	// ;
	public static final int TK_INT	        = 16;	// int
    public static final int TK_CONST	    = 17;	// const
	public static final int TK_COMMA        = 18;	// ,
	public static final int TK_TRUE         = 19;	// ture
	public static final int TK_FALSE        = 20;	// false
	public static final int TK_LT           = 21;	//<
	public static final int TK_LE           = 22;	//<=
	public static final int TK_GT           = 23;	//>
	public static final int TK_GE           = 24;	//>=
	public static final int TK_EQ           = 25;	//==
	public static final int TK_NE           = 26;	//!=








	public CToken(int type, int lineNo, int colNo, String s) {
		super(type, lineNo, colNo, s);
	}
}
