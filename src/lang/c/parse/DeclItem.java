package lang.c.parse;

import java.io.PrintStream;

import lang.*;
import lang.c.*;

public class DeclItem extends CParseRule {
    private CToken tk;
    private CType type;
    private int size;
    private CToken ident;
    private boolean isPointer = false;
    private boolean isArray = false;

    public DeclItem(CParseContext pcx) {
    }
    public static boolean isFirst(CToken tk) {
        return tk.getType() == CToken.TK_MULT || tk.getType() == CToken.TK_IDENT;
    }
    public void parse(CParseContext pcx) throws FatalErrorException {

        CTokenizer ct = pcx.getTokenizer();
        tk = ct.getCurrentToken(pcx);
        if (tk.getType() == CToken.TK_MULT) {
            isPointer = true;
            tk = ct.getNextToken(pcx);//Identが取り出さる
        }

        if(tk.getType() == CToken.TK_IDENT){
            ident = tk;
            tk = ct.getNextToken(pcx);//LBRA or 他が取り出される
        }else{
            pcx.fatalError(tk.toExplainString()+"後ろは識別子です");
        }

        if (tk.getType() == CToken.TK_LBRA) {
            isArray = true;
            tk = ct.getNextToken(pcx);//NUMが取り出される
            if (tk.getType() == CToken.TK_DECIMAL) {
                size = tk.getIntValue();
                tk = ct.getNextToken(pcx);//RBRAが取り出される
                if (tk.getType() == CToken.TK_RBRA) {
                    tk = ct.getNextToken(pcx);
                } else {
                    pcx.fatalError(tk.toExplainString()+"数字の後ろは']'です");
                }
            } else {
                pcx.fatalError(tk.toExplainString()+"'['後ろは数字です");
            }
        }

        if(isPointer){
            if(isArray){
                type = CType.getCType(CType.T_paint);
            }else{
                type = CType.getCType(CType.T_pint);
            }
        }else{
            if(isArray){
                type = CType.getCType(CType.T_aint);
            }else{
                type = CType.getCType(CType.T_int);
            }
        }
        CSymbolTable table = pcx.getSymbolTable();
        CSymbolTableEntry entry = new CSymbolTableEntry(type, size,false,true,0);
        if(!table.register(ident.getText(), entry)){
            pcx.fatalError(ident.toExplainString() + "多重定義です");
        }

    }

    public void semanticCheck(CParseContext pcx) throws FatalErrorException {
        //do nothing
    }

    public void codeGen(CParseContext pcx) throws FatalErrorException {
        PrintStream o = pcx.getIOContext().getOutStream();
        o.println(";;;  declItem starts");
        if(isArray){
            o.println(ident.getText() + ":\t.BLKW\t" + size +"; DecltItem: 配列宣言" + ident.toExplainString());
        }else{
            o.println(ident.getText() + ":\t.WORD\t0; DecltItem: 変数宣言" + ident.toExplainString());
        }
        o.println(";;;  declItem completes");
    }
}
