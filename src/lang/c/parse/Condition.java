package lang.c.parse;

import java.io.PrintStream;

import lang.*;
import lang.c.*;

public class Condition extends CParseRule {
    private CParseRule condition;
    private CParseRule left;
    private boolean bool;

    public Condition(CParseContext pcx) {
    }
    public static boolean isFirst(CToken tk) {
        return Expression.isFirst(tk) || tk.getType() == CToken.TK_TRUE || tk.getType() == CToken.TK_FALSE;
    }
    public void parse(CParseContext pcx) throws FatalErrorException {
        CTokenizer ct = pcx.getTokenizer();
        CToken tk = ct.getCurrentToken(pcx);
        if(Expression.isFirst(tk)){
            left = new Expression(pcx);
            left.parse(pcx);
            tk = ct.getCurrentToken(pcx);
            if(ConditionLT.isFirst(tk)){
                condition = new ConditionLT(pcx,left);
                condition.parse(pcx);
            }else if(ConditionLE.isFirst(tk)){
                condition = new ConditionLE(pcx,left);
                condition.parse(pcx);
            }else if(ConditionGT.isFirst(tk)){
                condition = new ConditionGT(pcx,left);
                condition.parse(pcx);
            }else if(ConditionGE.isFirst(tk)){
                condition = new ConditionGE(pcx,left);
                condition.parse(pcx);
            }else if(ConditionEQ.isFirst(tk)){
                condition = new ConditionEQ(pcx,left);
                condition.parse(pcx);
            }else if(ConditionNE.isFirst(tk)){
                condition = new ConditionNE(pcx,left);
                condition.parse(pcx);
            } else {
                pcx.fatalError(tk.toExplainString()+"expressionの後ろを確認してください");
            }
        }else if(tk.getType() == CToken.TK_TRUE){
            bool = true;
            ct.getNextToken(pcx);
        }else if(tk.getType() == CToken.TK_FALSE){
            bool = false;
            ct.getNextToken(pcx);
        }
    }

    public void semanticCheck(CParseContext pcx) throws FatalErrorException {
        if(condition != null) condition.semanticCheck(pcx);
    }

    public void codeGen(CParseContext pcx) throws FatalErrorException {
        PrintStream o = pcx.getIOContext().getOutStream();
        o.println(";;; condition starts");
        if(condition != null) condition.codeGen(pcx);
        else if(bool) o.println("\tMOV\t#0x0001, (R6)+\t; Condition");
        else o.println("\tMOV\t#0x0000, (R6)+\t; Condition");
        o.println(";;; condition completes");
    }
}

class ConditionLT extends CParseRule {
    private CParseRule left,right;
    private CToken op;

    public ConditionLT(CParseContext pcx, CParseRule left) {
        this.left = left;
    }
    public static boolean isFirst(CToken tk) {
        return tk.getType() == CToken.TK_LT;
    }
    public void parse(CParseContext pcx) throws FatalErrorException {
        CTokenizer ct = pcx.getTokenizer();
        CToken tk = ct.getCurrentToken(pcx);
        op = tk;
        tk = ct.getNextToken(pcx);
        if(Expression.isFirst(tk)) {
            right = new Expression(pcx);
            right.parse(pcx);
        } else {
            pcx.fatalError(tk.toExplainString()+"'<'後ろはexpressionです");
        }
    }

    public void semanticCheck(CParseContext pcx) throws FatalErrorException {
        if (left != null && right != null) {
            left.semanticCheck(pcx);
            right.semanticCheck(pcx);
            if (!left.getCType().equals(right.getCType())) {
                pcx.fatalError(op.toExplainString() +  "左辺の型 [" + left.getCType().toString() + "] と右辺の型 [" + right.getCType().toString() + "] が一致しないので比較できません");
            } else {
                this.setCType(CType.getCType(CType.T_bool));
                this.setConstant(true);
            } }
    }

    public void codeGen(CParseContext pcx) throws FatalErrorException {

        PrintStream o = pcx.getIOContext().getOutStream();
        o.println(";;; condition < (compare) starts");
        if (left != null && right != null) {
            left.codeGen(pcx);
            right.codeGen(pcx);
            int seq = pcx.getSeqId();
            o.println("\tMOV\t-(R6), R0\t; ConditionLT: 2数を取り出して、比べる");
            o.println("\tMOV\t-(R6), R1\t; ConditionLT:");
            o.println("\tMOV\t#0x0001, R2\t; ConditionLT: set true");
            o.println("\tCMP\tR0, R1\t; ConditionLT: R1<R0 = R1-R0<0");
            o.println("\tBRN\tLT" + seq + " ; ConditionLT:");
            o.println("\tCLR\tR2\t\t; ConditionLT: set false");
            o.println("LT" + seq + ":\tMOV\tR2, (R6)+\t; ConditionLT:");
        }
        o.println(";;;condition < (compare) completes");
    }
}

class ConditionLE extends CParseRule {
    private CParseRule left,right;
    private CToken op;

    public ConditionLE(CParseContext pcx, CParseRule left) {
        this.left = left;
    }
    public static boolean isFirst(CToken tk) {
        return tk.getType() == CToken.TK_LE;
    }
    public void parse(CParseContext pcx) throws FatalErrorException {
        CTokenizer ct = pcx.getTokenizer();
        CToken tk = ct.getCurrentToken(pcx);
        op = tk;
        tk = ct.getNextToken(pcx);
        if(Expression.isFirst(tk)) {
            right = new Expression(pcx);
            right.parse(pcx);
        } else {
            pcx.fatalError(tk.toExplainString()+"'<='後ろはexpressionです");
        }
    }

    public void semanticCheck(CParseContext pcx) throws FatalErrorException {
        if(left != null && right != null){
            left.semanticCheck(pcx);
            right.semanticCheck(pcx);
            if(!left.getCType().equals(right.getCType())){
                pcx.fatalError(op.toExplainString() + "左辺の型[" + left.getCType()+ "] と右辺の型["+ right.getCType() + "] が一致しないので比較できません");
            }else{
                this.setCType(CType.getCType(CType.T_bool));
                this.setConstant(true);
            }
        }
    }

    public void codeGen(CParseContext pcx) throws FatalErrorException {
        PrintStream o = pcx.getIOContext().getOutStream();
        o.println(";;; conditionLE starts");
        if(left != null && right != null){
            left.codeGen(pcx);
            right.codeGen(pcx);
            int seq = pcx.getSeqId();
            o.println("\tMOV\t-(R6), R0\t; ConditionLE: 2数を取り出して、比べる");
            o.println("\tMOV\t-(R6), R1\t; ConditionLE:");
            o.println("\tMOV\t#0x0001, R2\t; ConditionLE: set true");
            o.println("\tCMP\tR0, R1\t\t; ConditionLE: R1<=R0 = R1-R0<=0");
            o.println("\tBRN\tLE" + seq + "\t\t; ConditionLE:");
            o.println("\tBRZ\tLE" + seq + "\t\t; ConditionLE:");
            o.println("\tCLR\tR2\t\t; ConditionLE: set false");
            o.println("LE" + seq + ":\tMOV\tR2, (R6)+\t; ConditionLE:");
        }
        o.println(";;; conditionLE completes");
    }
}

class ConditionGT extends CParseRule {
    private CParseRule left,right;
    private CToken op;

    public ConditionGT(CParseContext pcx, CParseRule left) {
        this.left = left;
    }
    public static boolean isFirst(CToken tk) {
        return tk.getType() == CToken.TK_GT;
    }
    public void parse(CParseContext pcx) throws FatalErrorException {
        CTokenizer ct = pcx.getTokenizer();
        CToken tk = ct.getCurrentToken(pcx);
        op = tk;
        tk = ct.getNextToken(pcx);
        if(Expression.isFirst(tk)) {
            right = new Expression(pcx);
            right.parse(pcx);
        } else {
            pcx.fatalError(tk.toExplainString()+"'>'後ろはexpressionです");
        }
    }

    public void semanticCheck(CParseContext pcx) throws FatalErrorException {
        if(left != null && right != null){
            left.semanticCheck(pcx);
            right.semanticCheck(pcx);
            if(!left.getCType().equals(right.getCType())){
                pcx.fatalError(op.toExplainString() + "左辺の型[" + left.getCType()+ "] と右辺の型["+ right.getCType() + "] が一致しないので比較できません");
            }else{
                this.setCType(CType.getCType(CType.T_bool));
                this.setConstant(true);
            }
        }
    }

    public void codeGen(CParseContext pcx) throws FatalErrorException {
        PrintStream o = pcx.getIOContext().getOutStream();
        o.println(";;; conditionGT starts");
        if(left != null && right != null){
            left.codeGen(pcx);
            right.codeGen(pcx);
            int seq = pcx.getSeqId();
            o.println("\tMOV\t-(R6), R0\t; ConditionGT: 2数を取り出して、比べる");
            o.println("\tMOV\t-(R6), R1\t; ConditionGT:");
            o.println("\tMOV\t#0x0001, R2\t; ConditionGT: set true");
            o.println("\tCMP\tR1, R0\t\t; ConditionGT: R1>R0 = 0>R0-R1");
            o.println("\tBRN\tGT" + seq + "\t\t; ConditionGT:");
            o.println("\tCLR\tR2\t\t; ConditionGT: set false");
            o.println("GT" + seq + ":\tMOV\tR2, (R6)+\t; ConditionGT:");
        }
        o.println(";;; conditionGT completes");
    }
}

class ConditionGE extends CParseRule {
    private CParseRule left,right;
    private CToken op;

    public ConditionGE(CParseContext pcx, CParseRule left) {
        this.left = left;
    }
    public static boolean isFirst(CToken tk) {
        return tk.getType() == CToken.TK_GE;
    }
    public void parse(CParseContext pcx) throws FatalErrorException {
        CTokenizer ct = pcx.getTokenizer();
        CToken tk = ct.getCurrentToken(pcx);
        op = tk;
        tk = ct.getNextToken(pcx);
        if(Expression.isFirst(tk)) {
            right = new Expression(pcx);
            right.parse(pcx);
        } else {
            pcx.fatalError(tk.toExplainString()+"'>='後ろはexpressionです");
        }
    }

    public void semanticCheck(CParseContext pcx) throws FatalErrorException {
        if(left != null && right != null){
            left.semanticCheck(pcx);
            right.semanticCheck(pcx);
            if(!left.getCType().equals(right.getCType())){
                pcx.fatalError(op.toExplainString() + "左辺の型[" + left.getCType()+ "] と右辺の型["+ right.getCType() + "] が一致しないので比較できません");
            }else{
                this.setCType(CType.getCType(CType.T_bool));
                this.setConstant(true);
            }
        }
    }

    public void codeGen(CParseContext pcx) throws FatalErrorException {
        PrintStream o = pcx.getIOContext().getOutStream();
        o.println(";;; conditionGE starts");
        if(left != null && right != null){
            left.codeGen(pcx);
            right.codeGen(pcx);
            int seq = pcx.getSeqId();
            o.println("\tMOV\t-(R6), R0\t; ConditionGE: 2数を取り出して、比べる");
            o.println("\tMOV\t-(R6), R1\t; ConditionGE:");
            o.println("\tMOV\t#0x0001, R2\t; ConditionGE: set true");
            o.println("\tCMP\tR1, R0\t\t; ConditionGE: R1>=R0 = 0>=R0-R1");
            o.println("\tBRN\tGE" + seq + "\t\t; ConditionGE:");
            o.println("\tBRZ\tGE" + seq + "\t\t; ConditionGE:");
            o.println("\tCLR\tR2\t\t; ConditionGE: set false");
            o.println("GE" + seq + ":\tMOV\tR2, (R6)+\t; ConditionGE:");
        }
        o.println(";;; conditionGE completes");
    }
}

class ConditionEQ extends CParseRule {
    private CParseRule left,right;
    private CToken op;

    public ConditionEQ(CParseContext pcx, CParseRule left) {
        this.left = left;
    }
    public static boolean isFirst(CToken tk) {
        return tk.getType() == CToken.TK_EQ;
    }
    public void parse(CParseContext pcx) throws FatalErrorException {
        CTokenizer ct = pcx.getTokenizer();
        CToken tk = ct.getCurrentToken(pcx);
        op = tk;
        tk = ct.getNextToken(pcx);
        if(Expression.isFirst(tk)) {
            right = new Expression(pcx);
            right.parse(pcx);
        } else {
            pcx.fatalError(tk.toExplainString()+"'=='後ろはexpressionです");
        }
    }

    public void semanticCheck(CParseContext pcx) throws FatalErrorException {
        if(left != null && right != null){
            left.semanticCheck(pcx);
            right.semanticCheck(pcx);
            if(!left.getCType().equals(right.getCType())){
                pcx.fatalError(op.toExplainString() + "左辺の型[" + left.getCType()+ "] と右辺の型["+ right.getCType() + "] が一致しないので比較できません");
            }else{
                this.setCType(CType.getCType(CType.T_bool));
                this.setConstant(true);
            }
        }
    }

    public void codeGen(CParseContext pcx) throws FatalErrorException {
        PrintStream o = pcx.getIOContext().getOutStream();
        o.println(";;; conditionEQ starts");
        if(left != null && right != null){
            left.codeGen(pcx);
            right.codeGen(pcx);
            int seq = pcx.getSeqId();
            o.println("\tMOV\t-(R6), R0\t; ConditionEQ: 2数を取り出して、比べる");
            o.println("\tMOV\t-(R6), R1\t; ConditionEQ:");
            o.println("\tMOV\t#0x0001, R2\t; ConditionEQ: set true");
            o.println("\tCMP\tR0, R1\t\t; ConditionEQ: R1=R0 = R1-R0=0");
            o.println("\tBRZ\tEQ" + seq + "\t\t; ConditionEQ:");
            o.println("\tCLR\tR2\t\t; ConditionEQ: set false");
            o.println("EQ" + seq + ":\tMOV\tR2, (R6)+\t; ConditionEQ:");
        }
        o.println(";;; conditionEQ completes");
    }
}

class ConditionNE extends CParseRule {
    private CParseRule left,right;
    private CToken op;

    public ConditionNE(CParseContext pcx, CParseRule left) {
        this.left = left;
    }
    public static boolean isFirst(CToken tk) {
        return tk.getType() == CToken.TK_NE;
    }
    public void parse(CParseContext pcx) throws FatalErrorException {
        CTokenizer ct = pcx.getTokenizer();
        CToken tk = ct.getCurrentToken(pcx);
        op = tk;
        tk = ct.getNextToken(pcx);
        if(Expression.isFirst(tk)) {
            right = new Expression(pcx);
            right.parse(pcx);
        } else {
            pcx.fatalError(tk.toExplainString()+"'!='後ろはexpressionです");
        }
    }

    public void semanticCheck(CParseContext pcx) throws FatalErrorException {
        if(left != null && right != null){
            left.semanticCheck(pcx);
            right.semanticCheck(pcx);
            if(!left.getCType().equals(right.getCType())){
                pcx.fatalError(op.toExplainString() + "左辺の型[" + left.getCType()+ "] と右辺の型["+ right.getCType() + "] が一致しないので比較できません");
            }else{
                this.setCType(CType.getCType(CType.T_bool));
                this.setConstant(true);
            }
        }
    }

    public void codeGen(CParseContext pcx) throws FatalErrorException {
        PrintStream o = pcx.getIOContext().getOutStream();
        o.println(";;; conditionNE starts");
        if(left != null && right != null){
            left.codeGen(pcx);
            right.codeGen(pcx);
            int seq = pcx.getSeqId();
            o.println("\tMOV\t-(R6), R0\t; ConditionNE: 2数を取り出して、比べる");
            o.println("\tMOV\t-(R6), R1\t; ConditionNE:");
            o.println("\tCLR\tR2\t\t; ConditionNE: set false");
            o.println("\tCMP\tR0, R1\t\t; ConditionNE: R1!=R0 = R1-R0!=0");
            o.println("\tBRZ\tNE" + seq + "\t\t; ConditionNE:");
            o.println("\tMOV\t#0x0001, R2\t; ConditionNE: set true");
            o.println("NE" + seq + ":\tMOV\tR2, (R6)+\t; ConditionNE:");
        }
        o.println(";;; conditionNE completes");
    }
}


