package lang.c;
import lang.*;

import java.util.Iterator;

public class CSymbolTable {
    private class OneSymbolTable extends SymbolTable<CSymbolTableEntry> {
        @Override
        public CSymbolTableEntry register(String name, CSymbolTableEntry e) {
            return put(name, e);
        }
        @Override
        public CSymbolTableEntry search(String name) {
            return get(name);
        }

    }

    private OneSymbolTable global;// 大域変数用
    private OneSymbolTable local; // 局所変数用

    //コンストラクタ
    public CSymbolTable() {
        global = new OneSymbolTable();
        local = new OneSymbolTable();
    }

    public boolean register(String name, CSymbolTableEntry e) {
        if(global.get(name) != null){
            return false;
        }
        global.put(name, e);
        return true;
    }

    public CSymbolTableEntry search(String name) {
        return global.get(name);
    }

    public  void show() {
        global.show();
    }

}


