package lang.c.parse;

import java.io.PrintStream;

import lang.*;
import lang.c.*;

public class AddressToValue extends CParseRule {
    private CParseRule primary;
    public AddressToValue(CParseContext pcx) {
    }
    public static boolean isFirst(CToken tk) {
        return Primary.isFirst(tk);
    }
    public void parse(CParseContext pcx) throws FatalErrorException {
        primary = new Primary(pcx);
        primary.parse(pcx);
    }

    public void semanticCheck(CParseContext pcx) throws FatalErrorException {
        if (primary != null) {
            primary.semanticCheck(pcx);
            this.setCType(primary.getCType());		// primary の型をそのままコピー
            this.setConstant(primary.isConstant());
        }
    }

    public void codeGen(CParseContext pcx) throws FatalErrorException {
        PrintStream o = pcx.getIOContext().getOutStream();
        o.println(";;; AddressToValue starts");
        if(primary != null){
            primary.codeGen(pcx);
            o.println("\tMOV\t-(R6), R0\t; AddressToValue: スタックからアドレスを取り出し値に変換する");
            o.println("\tMOV\t(R0),(R6)+\t; AddressToValue");
        }
        o.println(";;; AddressToValue completes");
    }
}